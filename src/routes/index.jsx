import { Component } from "preact"
import { Router } from "preact-router"

// Code-splitting is automated for routes
import ChatPage from "./ChatPage"
import Error404 from "./Error404"

export default class Routes extends Component {
  handleRoute = e => {
    this.currentUrl = e.url;
  };

  render({ }, { }) {
    return (<div id="app">
      <Router onChange={this.handleRoute}>
        <ChatPage path="/" />
        <Error404 default={true} />
      </Router>
    </div>);
  }
}

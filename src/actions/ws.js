import {
  wsUrl
} from "../settings"

export function connect() {
  return (dispatch) => {
    var conn = new WebSocket("/ws")
    conn.onopen = () => {

    }
    conn.onerror = (err) => {
      console.log(err)
    }
    conn.onmessage = (msg) => {
      console.log(msg)
    }
    dispatch({
      type: "WEBSOCKET_PENDING",
      payload: conn
    })
  }
}

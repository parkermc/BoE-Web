export function loading(mode) {
  return {
    type: "LOADING",
    payload: mode
  }
}
